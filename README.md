Step1 - Clone the given repository in your local machine or instance or Pass the Git Master Repository URL in VerifyNow.
GIT Repo Name - https://gitlab.com/ramyavarrey/MagentoSuccessTest.git
Step2 - Make sure you have installed following software in your Instance or local Machine.
A - HP UFT14
B - Python2.x
Note : Make sure these two are installed in System(Windows-Path)


Step3 - Set the environment variable from command prompt.

A- Go to Run

B- Type Powershell.exe

C- Power Shell will be open.

D- C:\users  [Environment]::SetEnvironmentVariable("TEST_URL", "http://54.89.102.77/", "User");  press Enter

E- C:\users  [Environment]::SetEnvironmentVariable("BROWSER", "Chrome", "User")  press Enter

Note : Make sure you didn't get any error while pressed enter.


Step4 - Go to power shell again and run below command. ( Make sure you have setup Environment variable and run vbs file on same shell only)

PS C:> Cscript "Path of RunURLTest.vbs script where you have cloned from GIT " "Path of UFT test case folder " Press Enter

This will do following task.

A- Set the Report folder <"C:/users"> , where the reports will be generates.

B- Run the UFT test case from command prompt , this will use environment variable that we have passed in @step3.

C- Wait until UFT complete the test case , then it will start a Python converter file from command prompt that will convert reports from XML to Json.

Note : Repeat the same with Browser Type as Firefox or IE in Step 3





